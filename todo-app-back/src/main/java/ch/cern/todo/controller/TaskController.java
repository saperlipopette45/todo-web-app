package ch.cern.todo.controller;

import ch.cern.todo.dto.TaskDTO;
import ch.cern.todo.service.TaskService;
import jakarta.validation.Valid;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.Map;

@RestController
public class TaskController {

    private final TaskService taskService;

    public TaskController(TaskService taskService) {
        this.taskService = taskService;
    }

    @GetMapping("/tasks")
    public Iterable<TaskDTO> getTasks() {
        return taskService.getAllTasks();
    }

    @PostMapping("/tasks")
    public TaskDTO addTask(@RequestBody @Valid TaskDTO task) {
        return taskService.createNewTask(task);
    }

    @PutMapping("/tasks/{id}")
    public ResponseEntity<TaskDTO> updateTask(@PathVariable Long id, @RequestBody @Valid TaskDTO task) {
        task.setId(id);
        return ResponseEntity.ok(taskService.updateTask(task));
    }
    @DeleteMapping("/tasks/{id}")
    public ResponseEntity<Map<String, Boolean>> getAllTasks(@PathVariable Long id) {
        taskService.deleteTask(id);
        Map<String, Boolean> response = new HashMap<>();
        response.put("data", true);
        return ResponseEntity.ok(response);
    }
}