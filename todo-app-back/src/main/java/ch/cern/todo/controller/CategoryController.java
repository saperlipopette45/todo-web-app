package ch.cern.todo.controller;

import ch.cern.todo.dto.CategoryDTO;
import ch.cern.todo.service.CategoryService;
import jakarta.validation.Valid;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@RestController
public class CategoryController {

    private final CategoryService categoryService;

    public CategoryController(CategoryService categoryService) {
        this.categoryService = categoryService;
    }

    @GetMapping("/categories")
    public Iterable<CategoryDTO> getCategories() {
        return categoryService.getAllCategories();
    }

    @GetMapping("/categories/{id}")
    public Optional<CategoryDTO> getCategory(@PathVariable Long id) {
        return categoryService.findCategoryById(id);
    }

    @PostMapping("/categories")
    public CategoryDTO addCategory(@RequestBody @Valid CategoryDTO category) {
        return categoryService.createNewCategory(category);
    }

    @PutMapping("/categories/{id}")
    public ResponseEntity<Optional<CategoryDTO>> updateCategory(@PathVariable Long id, @RequestBody @Valid CategoryDTO category) {
        category.setId(id);
        categoryService.updateCategory(category);
        Optional<CategoryDTO> savedCategory = categoryService.findCategoryById(id);
        return ResponseEntity.ok(savedCategory);
    }
    @DeleteMapping("/categories/{id}")
    public ResponseEntity<Boolean> getAllCategories(@PathVariable Long id) throws Exception {
        categoryService.deleteCategory(id);
        return ResponseEntity.ok(true);
    }
}