package ch.cern.todo.service;


import ch.cern.todo.dto.CategoryDTO;

import java.util.Optional;

public interface CategoryService {

    public CategoryDTO createNewCategory(CategoryDTO task);
    public Iterable<CategoryDTO> getAllCategories();
    public Optional<CategoryDTO> findCategoryById(Long id);
    public void deleteCategory(Long id) throws Exception;
    public void updateCategory(CategoryDTO task);
}
