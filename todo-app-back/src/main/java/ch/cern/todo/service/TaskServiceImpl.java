package ch.cern.todo.service;

import ch.cern.todo.dto.TaskDTO;
import ch.cern.todo.entity.Task;
import ch.cern.todo.repository.TaskRepository;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class TaskServiceImpl implements TaskService {

    private final TaskRepository taskRepository;
    private final ModelMapper modelMapper;

    public TaskServiceImpl(
            TaskRepository taskRepository,
            ModelMapper modelMapper
    ) {
        this.taskRepository = taskRepository;
        this.modelMapper = modelMapper;
    }

    /**
     * Add a new Task
     */
    public TaskDTO createNewTask(TaskDTO taskDTO) {
        Task task = this.modelMapper.map(taskDTO, Task.class);
        Task savedTask = taskRepository.save(task);
        return modelMapper.map(savedTask, TaskDTO.class);
    }

    /**
     * Fetch all Tasks
     */
    public Iterable<TaskDTO> getAllTasks() {

        // Transform iterable to arraylist
        List<Task> result = new ArrayList<>();
        Iterable<Task> tasks = taskRepository.findAll();
        tasks.forEach(result::add);

        // Map results
        return result
                .stream()
                .map(task -> modelMapper.map(task, TaskDTO.class))
                .toList();
    }

    /**
     * Find Task by id
     */
    public Optional<TaskDTO> findTaskById(Long id) {
        Optional<Task> task = taskRepository.findById(id);
        return Optional.ofNullable(modelMapper.map(task, TaskDTO.class));
    }

    /**
     * Delete Task
     */
    public void deleteTask(Long id) {
        taskRepository.deleteById(id);
    }

    /**
     * Update Task
     */
    public TaskDTO updateTask(TaskDTO taskDTO) {
        Task task = this.modelMapper.map(taskDTO, Task.class);
        Task savedTask = taskRepository.save(task);
        return modelMapper.map(savedTask, TaskDTO.class);
    }

}
