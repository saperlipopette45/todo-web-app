package ch.cern.todo.service;

import ch.cern.todo.dto.CategoryDTO;
import ch.cern.todo.entity.Category;
import ch.cern.todo.entity.Task;
import ch.cern.todo.mapper.CategoryMapper;
import ch.cern.todo.repository.CategoryRepository;
import ch.cern.todo.repository.TaskRepository;
import jakarta.transaction.Transactional;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
@Transactional
public class CategoryServiceImpl implements CategoryService {

    private final CategoryRepository categoryRepository;
    private final TaskRepository taskRepository;

    public CategoryServiceImpl(
            CategoryRepository categoryRepository,
            TaskRepository taskRepository
    ) {
        this.categoryRepository = categoryRepository;
        this.taskRepository = taskRepository;
    }

    /**
     * Add a new Category
     */
    public CategoryDTO createNewCategory(CategoryDTO categoryDTO) {
        Category category = CategoryMapper.mapToEntity(categoryDTO);
        Category savedCategory = categoryRepository.save(category);
        return CategoryMapper.mapToDto(savedCategory);
    }

    /**
     * Fetch all Categories
     */
    public Iterable<CategoryDTO> getAllCategories() {

        // Transform iterable to arraylist
        List<Category> result = new ArrayList<>();
        Iterable<Category> categories = categoryRepository.findAll();
        categories.forEach(result::add);

        // Map results
        return result
                .stream()
                .map(CategoryMapper::mapToDto)
                .toList();
    }

    /**
     * Find Category by id
     */
    public Optional<CategoryDTO> findCategoryById(Long id) {
        Optional<Category> category = categoryRepository.findById(id);
        assert category.orElse(null) != null;
        return Optional.ofNullable(CategoryMapper.mapToDto(category.orElse(null)));
    }

    /**
     * Delete Category
     */
    public void deleteCategory(Long id) throws Exception {
        // Check if the category is linked to tasks
        List<Task> tasks = this.taskRepository.findByCategoryId(id);
        if(!tasks.isEmpty()){
            throw new Exception("Cannot delete category. It is attached to tasks");
        }else{
            categoryRepository.deleteById(id);
        }
    }

    /**
     * Update Category
     */
    public void updateCategory(CategoryDTO categoryDTO) {
        Category category = CategoryMapper.mapToEntity(categoryDTO);
        categoryRepository.save(category);
        // Reload the entity
        this.findCategoryById(categoryDTO.getId());
    }

}
