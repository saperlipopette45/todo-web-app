package ch.cern.todo.service;

import ch.cern.todo.dto.TaskDTO;

import java.util.Optional;

public interface TaskService {

    public TaskDTO createNewTask(TaskDTO task);
    public Iterable<TaskDTO> getAllTasks();
    public Optional<TaskDTO> findTaskById(Long id);
    public void deleteTask(Long id);
    public TaskDTO updateTask(TaskDTO task);
}
