package ch.cern.todo;

import ch.cern.todo.entity.Category;
import ch.cern.todo.entity.Task;
import ch.cern.todo.repository.CategoryRepository;
import ch.cern.todo.repository.TaskRepository;
import org.modelmapper.ModelMapper;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;

import java.text.SimpleDateFormat;
import java.util.stream.Stream;

@SpringBootApplication
public class TodoApplication {

	public static void main(String[] args) {
		SpringApplication.run(TodoApplication.class, args);
	}

	@Bean
	public ModelMapper modelMapper() {
		return new ModelMapper();
	}

	@Bean
	@ConditionalOnProperty(prefix = "job.autorun", name = "enabled", havingValue = "true", matchIfMissing = true)
	CommandLineRunner init(TaskRepository taskRepository, CategoryRepository categoryRepository) {
		return args -> {
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

			// Create categories
			Category itCategory = new Category("IT", "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua");
			Category hrCategory = new Category("Human resources", "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua");
			categoryRepository.save(itCategory);
			categoryRepository.save(hrCategory);

			// Create tasks
			Stream.of(
					new Task(null, "Update word template", "Review and update the word document used for the daily meetings", sdf.parse("2024-04-18"), itCategory),
					new Task(null, "Review #MR56", "Review Julian's merge request before the meeting of next week.", sdf.parse("2024-05-28"), itCategory),
					new Task(null, "New projet powerpoint", "Prepare the new project powerpoint for the breakfast meeting", sdf.parse("2024-05-11"), itCategory)
			).forEach(taskRepository::save);
		};
	}

}
