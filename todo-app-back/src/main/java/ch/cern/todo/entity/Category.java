package ch.cern.todo.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.*;
import jakarta.validation.Constraint;
import lombok.*;

import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Getter
@Setter
@Entity
public class Category {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column (name = "CATEGORY_ID", nullable = false)
    private Long id;

    @Column(name= "CATEGORY_NAME", length = 100, nullable = false, unique = true)
    private String name;

    @Column(name= "CATEGORY_DESCRIPTION", length = 500)
    private String description;

    @JsonIgnore
    @OneToMany(mappedBy = "category", cascade = CascadeType.ALL)
    private List<Task> tasks;

    public int countTasks(){
        if(tasks == null){
            return 0;
        }else{
            return this.tasks.toArray().length;
        }
    }

    public Category(String name, String description) {
        this.name = name;
        this.description = description;
    }
}
