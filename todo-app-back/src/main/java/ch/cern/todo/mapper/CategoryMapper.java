package ch.cern.todo.mapper;

import ch.cern.todo.dto.CategoryDTO;
import ch.cern.todo.entity.Category;

public class CategoryMapper {

    public static CategoryDTO mapToDto(Category category){

        CategoryDTO categoryDTO =  CategoryDTO.builder()
                .id(category.getId())
                .name(category.getName())
                .description(category.getDescription())
                .tasksCount(category.countTasks())
                .build();

        return categoryDTO;
    }


    public static Category mapToEntity(CategoryDTO categoryDTO){

        Category category = Category.builder()
                .id(categoryDTO.getId())
                .name(categoryDTO.getName())
                .description(categoryDTO.getDescription())
                .build();

        return category;

    }

}
