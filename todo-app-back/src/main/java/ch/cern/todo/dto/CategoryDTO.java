package ch.cern.todo.dto;

import ch.cern.todo.entity.Task;
import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.validation.constraints.NotBlank;
import lombok.*;

import java.util.List;

@NoArgsConstructor
@AllArgsConstructor
@Builder
@Setter
@Getter
public class CategoryDTO {

    private long id;
    @NotBlank(message = "Name cannot be empty")
    private String name;
    private String description;
    private int tasksCount;
}
