package ch.cern.todo.dto;

import ch.cern.todo.entity.Category;
import com.fasterxml.jackson.annotation.JsonFormat;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import lombok.*;

import java.util.Date;

@NoArgsConstructor
@AllArgsConstructor
@Builder
@Setter
@Getter
public class TaskDTO {

    private long id;
    @NotBlank(message = "Name cannot be empty")
    private String name;
    private String description;
    @NotNull()
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")
    private Date deadline;
    @NotNull()
    private Category category;
}
