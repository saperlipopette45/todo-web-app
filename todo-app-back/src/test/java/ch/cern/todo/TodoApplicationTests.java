package ch.cern.todo;

import ch.cern.todo.controller.CategoryController;
import ch.cern.todo.controller.TaskController;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest(properties = {"job.autorun.enabled=false"})
class TodoApplicationTests {

	@Autowired
	private TaskController taskController;

	@Autowired
	private CategoryController categoryController;

	@Test
	void contextLoads() {
		assertThat(taskController).isNotNull();
		assertThat(categoryController).isNotNull();
	}

}
