package ch.cern.todo.controller;


import ch.cern.todo.dto.TaskDTO;
import ch.cern.todo.entity.Category;
import ch.cern.todo.entity.Task;
import ch.cern.todo.service.TaskServiceImpl;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.time.Instant;
import java.util.Collections;
import java.util.Date;
import java.util.List;

@WebMvcTest(value = TaskController.class, properties = {"job.autorun.enabled=false"})
public class TaskControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ModelMapper modelMapper;

    @MockBean
    private TaskServiceImpl taskService;

    private final Category category =
            Category.builder()
                    .name("Test name")
                    .description("Test description")
                    .build();

    private final Task task =
            Task.builder()
                    .id((long) 1)
                    .name("Test name")
                    .description("Test description")
                    .deadline(Date.from(Instant.parse("2023-05-24T00:00:00.000Z")))
                    .category(category)
                    .build();

    private final List<Task> tasks = Collections.singletonList(task);

    @Test
    public void whenGetAllTasks_thenControlFlowCorrect() throws Exception {

        List<TaskDTO> tasksDTOResult = this.tasks
                .stream()
                .map(task -> modelMapper.map(task, TaskDTO.class))
                .toList();

        Mockito.when(this.taskService.getAllTasks()).thenReturn(tasksDTOResult);

        this.mockMvc.perform(MockMvcRequestBuilders.get("/tasks"))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(MockMvcResultMatchers.jsonPath("$").isArray())
                .andExpect(
                        MockMvcResultMatchers.jsonPath("$[0].id")
                                .value(this.task.getId())
                );

        Mockito.verify(this.taskService, Mockito.times(1)).getAllTasks();
    }

}
