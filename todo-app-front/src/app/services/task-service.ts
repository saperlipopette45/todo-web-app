import { Injectable, inject } from '@angular/core';
import { Task } from '../models/task';
import { Category } from '../models/category';
import ApiService from './api-service';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { TaskModalComponent } from '../components/task-modal/task-modal.component';
import { CategoryModalComponent } from '../components/category-modal/category-modal.component';

@Injectable({
    providedIn: 'root',
})
export default class TaskService {

    private tasks: Task[] = [];
    private categories: Category[] = []

    protected apiService = inject(ApiService);
    protected modalService = inject(NgbModal)

    constructor() {
        this.fetchCategories()
    }

    toggleTaskModal(task: Task | null = null) {
        const modalRef = this.modalService.open(TaskModalComponent);
        if (task) {
            modalRef.componentInstance.task = task;
        }
    }

    toggleCategoryModal(category: Category | null = null) {
        const modalRef = this.modalService.open(CategoryModalComponent);
        if (category) {
            modalRef.componentInstance.category = category;
        }
    }

    fetchCategories() {
        this.apiService.get('categories').then((categories: Category[]) => {
            this.categories = categories
        })
    }


    // Tasks methods
    addTask(task: Task) {
        this.tasks.push(task)
    }

    updateTask(id: number | null, task: Task) {
        const indexToUpdate = this.tasks.findIndex(obj => obj.id === id);
        if (indexToUpdate !== -1) {
            this.tasks[indexToUpdate] = task;
        }
    }

    removeTask(id: number | null){
        this.tasks = this.tasks.filter(obj => obj.id !== id)
    }

    setTasks(tasks: Task[]) {
        this.tasks = tasks
    }

    getTasks() {
        return this.tasks;
    }

    // Categories methods
    addCategory(category: Category) {
        this.categories.push(category)
    }

    updateCategory(id: number | null, category: Category) {
        const indexToUpdate = this.categories.findIndex(obj => obj.id === id);
        if (indexToUpdate !== -1) {
            this.categories[indexToUpdate] = category;
        }
    }

    removeCategory(id: number | null){
        this.categories = this.categories.filter(obj => obj.id !== id)
    }

    setCategories(categories: Category[]) {
        this.categories = categories
    }

    getCategories() {
        return this.categories;
    }

}