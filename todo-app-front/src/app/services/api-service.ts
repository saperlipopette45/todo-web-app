import axios from 'axios';
import { environment } from '../../environments/environment.development';
import { Injectable } from '@angular/core';

@Injectable({
    providedIn: 'root',
})
export default class ApiService {

    public post(action: string, params: any) {
        let configurationObject = this.getConfigurationObject('POST', action, params)
        return this.httpRequest(configurationObject);
    }

    public put(action: string, params: any) {
        let configurationObject = this.getConfigurationObject('PUT', action, params)
        return this.httpRequest(configurationObject);
    }

    public get(action: string) {
        let configurationObject = this.getConfigurationObject('GET', action)
        return this.httpRequest(configurationObject);
    }

    public delete(action: string) {
        let configurationObject = this.getConfigurationObject('DELETE', action)
        return this.httpRequest(configurationObject);
    }

    private getConfigurationObject(method: string, action: string, params = null) {
        let configurationObject: any = {
            method: method,
            url: environment.apiEndpoint + action,
            headers: { 'content-type': 'application/json' }
        };
        if (params) {
            configurationObject.data = JSON.stringify(params)
        }
        return configurationObject
    }

    private httpRequest(configurationObject: any): any {

        return new Promise(async (resolve, reject) => {
            axios(configurationObject).then(response => {
                if(response.status == 200){
                    resolve(response.data)
                }else{
                    reject(response)
                }
            }).catch(error => {
                reject(error)
            })
        });
    }

}