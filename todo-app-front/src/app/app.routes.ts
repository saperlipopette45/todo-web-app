import { Routes } from '@angular/router';
import { TasksHomeComponent } from './pages/tasks-home/tasks-home.component';
import { CategoriesHomeComponent } from './pages/categories-home/categories-home.component';

export const routes: Routes = [
    { path: '',   redirectTo: '/tasks', pathMatch: 'full' },
    { path: 'tasks', component: TasksHomeComponent },
    { path: 'categories', component: CategoriesHomeComponent },
];
