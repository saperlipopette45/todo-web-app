import { CommonModule } from '@angular/common';
import { Component, Input, inject } from '@angular/core';
import { FormControl, FormGroup, ReactiveFormsModule, Validators } from '@angular/forms';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import moment from 'moment';
import { Task } from '../../models/task';
import ApiService from '../../services/api-service';
import TaskService from '../../services/task-service';
import { ToastService } from '../../services/toast-service';
import { Category } from '../../models/category';

@Component({
  selector: 'app-category-modal',
  standalone: true,
  imports: [ReactiveFormsModule, CommonModule],
  templateUrl: './category-modal.component.html',
  styleUrl: './category-modal.component.scss'
})
export class CategoryModalComponent {

  modal = inject(NgbActiveModal)
  
  protected taskService = inject(TaskService)
  protected apiService = inject(ApiService)
  protected toastService = inject(ToastService)

  categoryForm!: FormGroup
  isNewCategory: boolean = true;
  loading: boolean = false;

  @Input() category: Category = {
    id: null,
    name: '',
    description: '',
    tasksCount: 0
  }

  ngOnInit(): void {
    this.isNewCategory = this.category.id === null
    this.categoryForm = new FormGroup({
      name: new FormControl(this.category.name, [
        Validators.required,
        Validators.minLength(3)
      ]),
      description: new FormControl(this.category.description)
    })
  }

  async onSubmit() {
    // Build payload
    const payload = {
      ...this.categoryForm.value
    }
    // Send request to API
    try {
      this.loading = true
      if (this.isNewCategory) {
        const categoryResult = await this.apiService.post("categories", payload)
        this.taskService.addCategory(categoryResult)
      } else {
        const categoryResult = await this.apiService.put("categories/" + this.category.id, payload)
        this.taskService.updateCategory(this.category.id, categoryResult)
      }
      this.loading = false
      this.toastService.show({ header: "Success", body: "The category has been saved successfully" })
      this.dismiss()
    } catch (error) {
      // TODO handle error
      this.toastService.show({ header: "Error", body: "The category could not be created" })
    }
  }

  dismiss(saveOnClose: boolean = false) {
    this.modal.dismiss()
  }

  // Form getter for name (used for displaying errors)
  get name() {
    return this.categoryForm.get('name');
  }
}
