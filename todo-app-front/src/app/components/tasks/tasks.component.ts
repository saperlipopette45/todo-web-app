import { Component, inject } from '@angular/core';
import { TaskModalComponent } from '../task-modal/task-modal.component';
import { Task } from '../../models/task';
import { CommonModule } from '@angular/common';
import { TaskCardComponent } from '../task-card/task-card.component';
import ApiService from '../../services/api-service';
import TaskService from '../../services/task-service';

@Component({
  selector: 'app-tasks',
  standalone: true,
  imports: [TaskModalComponent, CommonModule, TaskCardComponent],
  templateUrl: './tasks.component.html',
  styleUrl: './tasks.component.scss'
})
export class TasksComponent {

  protected apiService = inject(ApiService);
  protected taskService = inject(TaskService);


  fetchTasks() {
    this.apiService.get('tasks').then((tasks: Task[]) => {
      this.taskService.setTasks(tasks)
    })
  }

  trackTask(index: number, task: Task) {
    return task.id;
  }

  ngOnInit() {
    this.fetchTasks()
  }

  openModal() {
    this.taskService.toggleTaskModal()
  }

  get tasks(): Array<Task> {
    return this.taskService.getTasks()
  }

}
