import { Component, Input, inject } from '@angular/core';
import { NgbActiveModal, NgbDatepickerModule } from '@ng-bootstrap/ng-bootstrap';
import { Task } from '../../models/task';
import { FormControl, FormGroup, ReactiveFormsModule, Validators } from '@angular/forms';
import TaskService from '../../services/task-service';
import { CommonModule } from '@angular/common';
import { formatDate } from '@angular/common'
import ApiService from '../../services/api-service';
import { ToastService } from '../../services/toast-service';
import moment from 'moment';

@Component({
  selector: 'app-task-modal',
  standalone: true,
  imports: [NgbDatepickerModule, ReactiveFormsModule, CommonModule],
  templateUrl: './task-modal.component.html',
  styleUrl: './task-modal.component.scss'
})
export class TaskModalComponent {

  modal = inject(NgbActiveModal)
  protected taskService = inject(TaskService)
  protected apiService = inject(ApiService)
  protected toastService = inject(ToastService)

  taskForm!: FormGroup
  isNewTask: boolean = true;
  loading: boolean = false;

  taskCategories() {
    return this.taskService.getCategories()
  }

  @Input() task: Task = {
    id: null,
    name: '',
    description: '',
    deadline: '',
    category: undefined,
    categoryId: null
  }

  ngOnInit(): void {
    this.isNewTask = this.task.id === null
    this.taskForm = new FormGroup({
      name: new FormControl(this.task.name, [
        Validators.required,
        Validators.minLength(3)
      ]),
      description: new FormControl(this.task.description),
      deadline: new FormControl(this.task.deadline),
      categoryId: new FormControl(this.task.categoryId),
    })
    // Set categoryId manually
    if (this.task.category) {
      this.taskForm.patchValue({ categoryId: this.task.category.id })
    }
    // Set date field
    if (this.task.deadline) {
      console.log(this.task.deadline)
      this.taskForm.patchValue({ deadline: new Date(this.task.deadline).toISOString().slice(0, 16) })
    }
  }

  async onSubmit() {
    // Build payload
    const payload = {
      ...this.taskForm.value,
      deadline: moment(this.taskForm.get("deadline")?.value).format("yyyy-MM-d HH:mm:ss"),
      category: {
        id: this.taskForm.get("categoryId")?.value
      }
    }
    // Send request to API
    try {
      this.loading = true
      if (this.isNewTask) {
        const taskResult = await this.apiService.post("tasks", payload)
        this.taskService.addTask(taskResult)
      } else {
        const taskResult = await this.apiService.put("tasks/" + this.task.id, payload)
        this.taskService.updateTask(this.task.id, taskResult)
      }
      this.loading = false
      this.toastService.show({ header: "Success", body: "The task has been saved successfully" })
      this.dismiss()
    } catch (error) {
      // TODO handle error
      this.toastService.show({ header: "Error", body: "The task could not be created" })
    }
  }

  dismiss(saveOnClose: boolean = false) {
    this.modal.dismiss()
  }

  // Form getter for name (used for displaying errors)
  get name() {
    return this.taskForm.get('name');
  }
}
