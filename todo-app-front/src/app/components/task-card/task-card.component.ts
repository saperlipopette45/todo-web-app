import { Component, Input, inject } from '@angular/core';
import { Task } from '../../models/task';
import moment from 'moment';
import TaskService from '../../services/task-service';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import ApiService from '../../services/api-service';
import { ToastService } from '../../services/toast-service';

@Component({
  selector: 'app-task-card',
  standalone: true,
  imports: [],
  templateUrl: './task-card.component.html',
  styleUrl: './task-card.component.scss'
})
export class TaskCardComponent {

  protected modalService = inject(NgbModal)
  protected taskService = inject(TaskService)
  protected apiService = inject(ApiService)
  protected toastService = inject(ToastService)

  @Input()
  task!: Task;

  delete() {

  }

  editTask() {
    let taskModal = { ...this.task }
    this.taskService.toggleTaskModal(taskModal)
  }

  async deleteTask() {
    try {
      await this.apiService.delete(`tasks/${this.task.id}`)
      this.taskService.removeTask(this.task.id)
      this.toastService.show({ header: 'Success', body: 'Task has been deleted' })
    } catch (error) {
      this.toastService.show({ header: 'Error', body: 'Could not delete task' })
    }
  }

  getAvailableCategories() {
    return this.taskService.getCategories()
  }

  taskDeadlineRelative() {
    return moment(this.task.deadline).fromNow()
  }

  taskDeadlineDate() {
    return moment(this.task.deadline).format('LLL')
  }

}
