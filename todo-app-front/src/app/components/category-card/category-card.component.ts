import { Component, Input, inject } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import moment from 'moment';
import { Task } from '../../models/task';
import ApiService from '../../services/api-service';
import TaskService from '../../services/task-service';
import { ToastService } from '../../services/toast-service';
import { Category } from '../../models/category';
import { CommonModule } from '@angular/common';

@Component({
  selector: 'app-category-card',
  standalone: true,
  imports: [CommonModule],
  templateUrl: './category-card.component.html',
  styleUrl: './category-card.component.scss'
})
export class CategoryCardComponent {

  protected modalService = inject(NgbModal)
  protected taskService = inject(TaskService)
  protected apiService = inject(ApiService)
  protected toastService = inject(ToastService)

  @Input()
  category!: Category;

  delete() {

  }

  editCategory() {
    let categoryModal = { ...this.category }
    this.taskService.toggleCategoryModal(categoryModal)
  }

  async deleteCategory() {
    try {
      await this.apiService.delete(`categories/${this.category.id}`)
      this.taskService.removeCategory(this.category.id)
      this.toastService.show({ header: 'Success', body: 'Category has been deleted' })
    } catch (error) {
      this.toastService.show({ header: 'Error', body: 'Could not delete category' })
    }
  }

  getAvailableCategories() {
    return this.taskService.getCategories()
  }

}
