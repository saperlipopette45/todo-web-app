import { Category } from "./category";

export interface Task {
  id: number | null;
  name: string;
  description: string;
  deadline: string;
  category: Category | undefined;
  categoryId: number | null;
}