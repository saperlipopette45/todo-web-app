import { Component, inject } from '@angular/core';
import { CategoryCardComponent } from '../../components/category-card/category-card.component';
import TaskService from '../../services/task-service';
import { CommonModule } from '@angular/common';

@Component({
  selector: 'app-categories-home',
  standalone: true,
  imports: [CategoryCardComponent, CommonModule],
  templateUrl: './categories-home.component.html',
  styleUrl: './categories-home.component.scss'
})
export class CategoriesHomeComponent {

  protected taskService = inject(TaskService)

  createCategory() {
    this.taskService.toggleCategoryModal()
  }

  get categories(){
    return this.taskService.getCategories()
  }

}
