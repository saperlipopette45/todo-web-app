import { Component, inject } from '@angular/core';
import TaskService from '../../services/task-service';
import { TasksComponent } from '../../components/tasks/tasks.component';

@Component({
  selector: 'app-tasks-home',
  standalone: true,
  imports: [TasksComponent],
  templateUrl: './tasks-home.component.html',
  styleUrl: './tasks-home.component.scss'
})
export class TasksHomeComponent {

  protected taskService = inject(TaskService)

  createTask() {
    this.taskService.toggleTaskModal()
  }

  getTasks() {
    return this.taskService.getTasks()
  }

}
