import { Component, inject } from '@angular/core';
import { RouterOutlet } from '@angular/router';
import { TasksComponent } from './components/tasks/tasks.component';
import { NavbarComponent } from './components/navbar/navbar.component';
import { FooterComponent } from './components/footer/footer.component';
import { TaskModalComponent } from './components/task-modal/task-modal.component';
import { CommonModule } from '@angular/common';
import TaskService from './services/task-service';
import { ToastsComponent } from './components/toasts/toasts.component';
import { TasksHomeComponent } from './pages/tasks-home/tasks-home.component';

@Component({
  selector: 'app-root',
  standalone: true,
  imports: [
    RouterOutlet,
    TasksComponent,
    NavbarComponent,
    FooterComponent,
    ToastsComponent
  ],
  templateUrl: './app.component.html',
  styleUrl: './app.component.scss'
})
export class AppComponent {

  

}
