# Todo web app

This repo contains a basic web application of a TODO list. The application is composed of :

- A back-end in SpringBoot using H2 database. Data is exposed via a REST API
- A front-end in Angular

## Setup

### Back-end

1. Open the project with Eclise or IntelliJ and let the IDE sync your gradle file

2. Build the project 
```bash
./gradlew clean build
```

3. Run the project 
```bash
./gradlew bootRun
```

i. Cors have been disabled for development purpose only

### Front-end

1. Install the dependencies  
```bash
npm install
```
or

```bash
yarn
```

2. Set the API endpoint  
Open `todo-app-front/src/environments/environment.development.ts` and set the apiEndpoint

3. Run the project
Run the app with

```bash
ng serve
```